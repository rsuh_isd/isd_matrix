#include <vector>
#include <limits>
#include <iostream>

#ifndef MATRIX_H
#define MATRIX_H
class Matrix
{

private:
    std::vector<int> data;
    int rows_qtty;
    int cols_qtty;//can be negative for error matrix!
    static int error_cell;
    static Matrix error_matrix;
    static void cleanup_errors();
    class Row
    {
    private:
        Matrix& parent;
        int row;

    public:
        int & operator[] (int col);//by reference: returns a ref to an actual part of matrix that can be modified
        Row(Matrix& m, int r):parent(m), row(r) {};
    };
    Matrix();
    Matrix& operator=(const Matrix&);
    Matrix(const Matrix& rhs);
public:
    void print();
    Matrix(int rows, int cols);
    Row operator[] (int row);// not by reference: returns a temp object anyway

};


#endif

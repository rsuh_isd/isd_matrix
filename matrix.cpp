#include "matrix.h"
#include <iostream>

int Matrix::error_cell=std::numeric_limits<int>::min();
Matrix Matrix::error_matrix(-1,-1);

void Matrix::cleanup_errors()
{
    Matrix::error_cell=std::numeric_limits<int>::min();
    error_matrix.data.clear();
    error_matrix.rows_qtty=-1;
    error_matrix.cols_qtty=-1;
}

Matrix::Matrix(int rows, int cols):rows_qtty(rows), cols_qtty(cols)
{
    using namespace std;
    int dummy_value=0;
    for (int i=0; i<rows*cols; i++)
    {
        this->data.push_back(dummy_value);
        dummy_value++;
    }
}

void Matrix::print()
{
    using namespace std;
    clog << "printing out matrix, size: " << cols_qtty << " cols, " << rows_qtty << " rows" << endl;
    for (int i=0; i<rows_qtty; i++)
    {
        for (int j=0; j<cols_qtty; j++)
        {
            cout << data[i*cols_qtty+j];
        }
        cout << endl;
    }
}

int & Matrix::Row::operator[] (int i)
{
    using namespace std;
    Matrix::cleanup_errors();
    if (i>=parent.cols_qtty)
    {
        cerr << "Index out of bound: no column " << i << " in a matrix " << parent.rows_qtty << " rows x " << parent.cols_qtty << " cols" << endl;
        return error_cell;
    }
    return parent.data[row*parent.rows_qtty+i];
}


Matrix::Row Matrix::operator[] (int row)
{
    using namespace std;
    if (row>=this->rows_qtty)
    {
        cerr << "Index out of bound: no row " << row << " in a matrix " << this->rows_qtty << " rows x " << this->cols_qtty << " cols" << endl;
        return Row(error_matrix, 0);
    }
    return Row(*this, row);
}

#include <iostream>
#include "matrix.h"
int main()
{
    using namespace std;
    Matrix matrix(2, 2);
    matrix.print();

    cout << endl << "testing operator[][]" << endl;
    matrix[0][1]=9;
    matrix[2][1]=5;
    matrix.print();
    cout << matrix[2][1] << endl;

    return 0;
}
